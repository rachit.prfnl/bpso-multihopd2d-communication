function [ o ] = ObjectiveFunction1 (x)

%% Initialization section
% clf
% clear all
 cu_x=[1,2,2,2,2,4,5,5,6,6,7,4,8,4,8];
 cu_y=[2,2,3,4,5,6,2,3,4,5,6,3,7,7,3];
 bs=[5 5];
 pcu=x(1);
 penb=21;
%% Dsitance calculation 

dist_cb=zeros(1,15);

for i=1:15
    dist_cb(i)=sqrt( (cu_x(i)-bs(1))^2 + ...
                     (cu_y(i)-bs(2))^2);
    
    cu(i).id=i;
    cu(i).x=cu_x(i);
    cu(i).y=cu_y(i);
    cu(i).distanceBS=dist_cb(i);
end
%% Gain
% Cellular users gain
for i=1:15 
    g_cu(i)=(128.1+37.6*log10(dist_cb(i)));
    cu(i).GainBS=g_cu(i);
    cu(i).noise=-174;
    cu(i).SINR=(pcu*g_cu(i))/cu(i).noise;
    cu(i).Throughput=real(log2(1+cu(i).SINR));
end
%% finding coordinates in range
receiver= [7 6];
sender=[5 2];



for i=1:15
    if(cu(i).x<7 && cu(i).x>4 && cu(i).y>2 && cu(i).y<6)
        cu(i).Relay='Yes';
    else
        cu(i).Relay='No';
    end
end


hold on 

scatter(bs(1),bs(2));

scatter(cu_x,cu_y,'filled')
xlim([0 10])
ylim([0 10])
scatter(5,2,'Green','filled')
text(5.1,2.1,'Sender');
scatter(7,6,'Green','filled')
text(7.1,6.1,'Receiver');
title('Mode selection : Multihop');
xlabel('longitude coordinate');
ylabel('latitude coordinate');
grid on;
legend('Base Station','User positions');

count=1;

x_mat(1)=sender(1);
y_mat(1)=sender(2);

mat=2;
for i=1:15
       
    if(strcmp(cu(i).Relay,'Yes')==1)
         scatter(cu(i).x,cu(i).y,'Blue','filled')
       text(cu(i).x+0.1,cu(i).y+0.1,strcat('Relay',num2str(count)));
        x_mat(mat)=cu(i).x;
        y_mat(mat)=cu(i).y;
        mat=mat+1;
        count=count+1;
    end
end
x_mat(mat)=receiver(1);
y_mat(mat)=receiver(2);


pd2d=x(2);

for i=1:4
    dist=sqrt((x_mat(i+1)-x_mat(i))^2 + (y_mat(i+1)-y_mat(i))^2);
    g_d2d(i)=(148+40*log10(dist));
end

relaymin=min(g_d2d);
put=1;
for i=1:15
    if(cu(i).x==sender(1)&&cu(i).y==sender(2))
    g_gainbs(put)=cu(i).GainBS;
    put=put+1;
    elseif (cu(i).x==receiver(1)&&cu(i).y==receiver(2))
    g_gainbs(put)=cu(i).GainBS;
    put=put+1;
    end
end
relaybsmin=min(g_gainbs);
 text (bs(1)-1.5,bs(2)+0.5,strcat('Min Gain :',num2str(relaybsmin),'dBm'));
 text (sender(1)-0.9,sender(2)-0.4,strcat('Min Gain :',num2str(relaymin),'dBm'));

if (relaymin<relaybsmin)
    disp('Choose Cellular mode'); 
     text(0.1,0.5,'Recommendation:Choose Cellular mode')
else
    disp('Choose D2d Mode');
     text(0.1,0.5,'Recommendation:Choose D2D mode')
end

 hold off
 
 %% D2d pair formation 
 
 for i=1:4
     
     d2d_tx(i)=x_mat(i);
     d2d(i).tx=d2d_tx(i); %D2D structure
     d2d_ty(i)=y_mat(i);
     d2d(i).ty=d2d_ty(i); % D2D structure
     
     d2d_rx(i)=x_mat(i+1); 
     d2d(i).rx=d2d_rx(i); % structure
     d2d_ry(i)=y_mat(i+1);
     d2d(i).ry=d2d_ry(i); % structure
 end
 
%intmatrix=zeros(15,4);
for i=1:15  % making sender and receiver relay section in structure as YES
    if(cu(i).x==7&& cu(i).y==6)
        cu(i).Relay='Yes';
    elseif(cu(i).x==sender(1)&& cu(i).y==sender(2))
        cu(i).Relay='Yes';
    end
end

for i=1:15
    if(strcmp(cu(i).Relay,'No')==1 )  
        for j=1:4
            dist=sqrt( (cu(i).x-d2d_tx(j))^2 + (cu(i).y-d2d_ty(j))^2 );
            gainnew=148+40*log10(dist);
            inter = pd2d*gainnew;
            intmatrix(i,j)=(penb*g_cu(i)) / ( cu(i).noise + inter); 
        end
    else
        for j=1:4
            intmatrix(i,j)=99999;
        end
    end
end

[assignment,cost] = munkres(intmatrix);

for i=1:size(assignment,2)
    if (assignment(i)~=0)
        d2d(assignment(i)).RB=i;
    end
end
o= cost;
end